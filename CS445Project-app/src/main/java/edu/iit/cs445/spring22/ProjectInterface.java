package edu.iit.cs445.spring22;

import java.util.List;
import java.util.UUID;

public interface ProjectInterface {
	
	List<BuyNothing> getAllAccounts();
	BuyNothing createAccount(BuyNothing id);
	BuyNothing createAsk(BuyNothing id);
	BuyNothing getAskId(String askId);
	
}
