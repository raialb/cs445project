package edu.iit.cs445.spring22;

import java.util.UUID;

import com.google.gson.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

// import jakarta.annotation.PostConstruct;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.*;

// Path declaration: https://localhost:8080/bn/api/demo/accounts
// make others for asks, gives, thanks, notes, reports

@Path("/demo")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class REST_Accounts {

	private ProjectInterface projInterface = new BuyNothingManager();
	
	@Path("/accounts")
	@POST
	public Response createAccount(@Context UriInfo uriInfo, String json) {
		String id;
		
		Gson gs = new Gson();
		BuyNothing iacc = gs.fromJson(json, BuyNothing.class);
		BuyNothing acc = projInterface.createAccount(iacc);
		
		id = acc.getID();
		Gson gson = new Gson();
		String s = gson.toJson(acc);
		
		UriBuilder builder = uriInfo.getAbsolutePathBuilder();
		builder.path(id.toString());
		
		// Response includes header and body data
		return Response.created(builder.build()).entity(s).build();
	}
	
	@Path("/accounts")
	@GET
	public Response getAllAccounts() {
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String str = gson.toJson(projInterface.getAllAccounts());
		
		return Response.status(Response.Status.OK).entity(str).build();
	}
	
}
