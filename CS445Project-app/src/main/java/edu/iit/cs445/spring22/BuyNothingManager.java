package edu.iit.cs445.spring22;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.NoSuchElementException;

public class BuyNothingManager implements ProjectInterface {
	
	private static List<BuyNothing> Accounts = new ArrayList<BuyNothing>();
	private static List<BuyNothing> Asks = new ArrayList<BuyNothing>();
	
	public List<BuyNothing> getAllAccounts() {
		return(Accounts);
	}
	
	public BuyNothing createAccount(BuyNothing acc) {
		BuyNothing account = new BuyNothing(acc);
		Accounts.add(account);
		return(account);
	}
	
	public BuyNothing createAsk(BuyNothing ask) {
		BuyNothing a = new BuyNothing(ask);
		Accounts.add(a);
		return(a);
	}
	
	public BuyNothing getAskId(String askId) {
		return findById(askId);
	}
	
	public BuyNothing findById(String id) {
		System.out.println(id);
		Iterator<BuyNothing> bnIter = BuyNothing.listIterator();
		while(bnIter.hasNext()) {
			BuyNothing bN = bnIter.next();
			if(bN.matchesId(id)) 
				return(bN);
		}
		return(new NullBN());
	}
	
}
